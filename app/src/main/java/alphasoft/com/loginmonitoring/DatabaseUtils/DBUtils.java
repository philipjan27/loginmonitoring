package alphasoft.com.loginmonitoring.DatabaseUtils;

import android.content.Context;
import android.util.Log;

import java.util.List;

import alphasoft.com.loginmonitoring.Api.APIClient;
import alphasoft.com.loginmonitoring.Api.APIInteface;


import alphasoft.com.loginmonitoring.Pojo.Departments;
import alphasoft.com.loginmonitoring.Pojo.Employee;
import alphasoft.com.loginmonitoring.Pojo.Room;
import alphasoft.com.loginmonitoring.Pojo.Schedule;
import alphasoft.com.loginmonitoring.Pojo.Subjects;
import alphasoft.com.loginmonitoring.Pojo.Teachers;
import alphasoft.com.loginmonitoring.Pojo.Transacrions;
import alphasoft.com.loginmonitoring.Utilities.Utils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by PhilipJan on 8/1/2017.
 */

public class DBUtils {

    private final static String TAG = DBUtils.class.getSimpleName();
    APIClient client;
    Context ctx;


    List<Departments> departmentsList;
    List<Employee> employeeList;
    List<Room> roomList;
    List<Schedule> scheduleList;
    List<Subjects> subjectsList;
    List<Teachers> teachersList;
    List<Transacrions> transacrionsList;


    public DBUtils(Context context) {
        this.ctx = context;
    }
    

    public void initializeAllRequest() {
        client = APIInteface.getClient().create(APIClient.class);

        Call<List<Departments>> departmentsCall= client.getDepartMentList();
        departmentsCall.enqueue(new Callback<List<Departments>>() {
            @Override
            public void onResponse(Call<List<Departments>> call, Response<List<Departments>> response) {
                Log.d(TAG, "Departments: "+response.body().size());
                departmentsList= response.body();
                Utils.saveDepartmentsToDb(departmentsList, ctx);
            }

            @Override
            public void onFailure(Call<List<Departments>> call, Throwable t) {
                Log.e(TAG, "Departments: "+t.getLocalizedMessage());
            }
        });

        Call<List<Employee>> employeeCall = client.getEmployeeList();
        employeeCall.enqueue(new Callback<List<Employee>>() {
            @Override
            public void onResponse(Call<List<Employee>> call, Response<List<Employee>> response) {
                employeeList= response.body();
                Log.d(TAG,"Employee: "+employeeList.size());
                Utils.saveEmployeeToDb(employeeList,ctx);
            }

            @Override
            public void onFailure(Call<List<Employee>> call, Throwable t) {
                Log.e(TAG, "Employee: " + t.toString());
            }
        });

        Call<List<Room>> roomCall = client.getRoomList();
        roomCall.enqueue(new Callback<List<Room>>() {
            @Override
            public void onResponse(Call<List<Room>> call, Response<List<Room>> response) {
                roomList= response.body();
                Log.d(TAG, "Rooms: " + response.body().toString());
                Log.d(TAG, "Rooms: SIZE" + roomList.size());
                Utils.saveRoomToDb(roomList,ctx);

            }

            @Override
            public void onFailure(Call<List<Room>> call, Throwable t) {
                Log.e(TAG, "Rooms: " + t.toString());
            }
        });

        Call<List<Schedule>> scheduleCall = client.getScheduleList();
        scheduleCall.enqueue(new Callback<List<Schedule>>() {
            @Override
            public void onResponse(Call<List<Schedule>> call, Response<List<Schedule>> response) {
                Log.d(TAG, "Schedule Response: " + response.body().get(0).getStartTime());
                Log.d(TAG, "Schedule Response: " + response.body().toString());
                scheduleList= response.body();
                Utils.saveScheduleToDb(scheduleList,ctx);
                Log.d(TAG, "Schedules: Size"+String.valueOf(scheduleList.size()));
            }

            @Override
            public void onFailure(Call<List<Schedule>> call, Throwable t) {
                Log.e(TAG, "Schedules: " + t.fillInStackTrace());
            }
        });

        Call<List<Subjects>> subjectsCall = client.getSubjectList();
        subjectsCall.enqueue(new Callback<List<Subjects>>() {
            @Override
            public void onResponse(Call<List<Subjects>> call, Response<List<Subjects>> response) {
                subjectsList= response.body();
                Log.d(TAG, "Subjects: " + response.body().toString());
                Log.d(TAG, "Subjects: SIZE" + response.body().size());
                Utils.saveSubjectsToDb(subjectsList,ctx);
            }

            @Override
            public void onFailure(Call<List<Subjects>> call, Throwable t) {
                Log.e(TAG, "Subjects: " + t.toString());
            }
        });

        Call<List<Teachers>> teachersCall = client.getTeachersList();
        teachersCall.enqueue(new Callback<List<Teachers>>() {
            @Override
            public void onResponse(Call<List<Teachers>> call, Response<List<Teachers>> response) {
                Log.d(TAG, "Teachers: " + response.body().toString());
                teachersList= response.body();
                Log.d(TAG, "Teachers: Size" +teachersList.size());
                Utils.saveTeacherToDb(teachersList,ctx);


            }

            @Override
            public void onFailure(Call<List<Teachers>> call, Throwable t) {
                Log.e(TAG, "Teachers: " + t.toString());
            }
        });

    }
}
