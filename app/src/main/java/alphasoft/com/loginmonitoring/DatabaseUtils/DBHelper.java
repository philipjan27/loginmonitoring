package alphasoft.com.loginmonitoring.DatabaseUtils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import alphasoft.com.loginmonitoring.Pojo.Departments;
import alphasoft.com.loginmonitoring.Pojo.Employee;
import alphasoft.com.loginmonitoring.Pojo.QueryMapperPojo;
import alphasoft.com.loginmonitoring.Pojo.Room;
import alphasoft.com.loginmonitoring.Pojo.Schedule;
import alphasoft.com.loginmonitoring.Pojo.Subjects;
import alphasoft.com.loginmonitoring.Pojo.Teachers;
import alphasoft.com.loginmonitoring.Pojo.Transacrions;
import alphasoft.com.loginmonitoring.R;
import alphasoft.com.loginmonitoring.Utilities.Constants;

/**
 * Created by PhilipJan on 7/25/2017.
 */

public class DBHelper extends OrmLiteSqliteOpenHelper {


    private Dao<Departments, Integer> departmentsDao;
    private Dao<Employee, Integer> employeeDao;
    private Dao<Room, Integer> roomDao;
    private Dao<Schedule, Integer> schedulesDao;
    private Dao<Subjects, Integer> subjectsDao;
    private Dao<Teachers, Integer> teachersDao;
    private Dao<Transacrions, Integer> transactionsDao;
    private Dao<QueryMapperPojo, Integer> queryMapperPojos;


    public DBHelper(Context context) {
        super(context, Constants.DBNAME, null, Constants.DBVERSION, R.raw.ormlite_config);
    }


    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {

        try {
            TableUtils.createTable(connectionSource, Departments.class);
            TableUtils.createTable(connectionSource, Employee.class);
            TableUtils.createTable(connectionSource, Room.class);
            TableUtils.createTable(connectionSource, Schedule.class);
            TableUtils.createTable(connectionSource, Subjects.class);
            TableUtils.createTable(connectionSource, Teachers.class);
            TableUtils.createTable(connectionSource, Transacrions.class);
            TableUtils.createTable(connectionSource, QueryMapperPojo.class);

        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {

    }

    public Dao<Departments, Integer> getDepartmentsDao() throws java.sql.SQLException {
        if (departmentsDao == null) {
            departmentsDao= getDao(Departments.class);
        }

        return departmentsDao;
    }

    public Dao<Employee, Integer> getEmployeeDao() throws java.sql.SQLException {
        if (employeeDao == null) {
            employeeDao= getDao(Employee.class);
        }

        return employeeDao;
    }

    public Dao<Room, Integer> getRoomDao() throws java.sql.SQLException {
        if (roomDao == null) {
            roomDao= getDao(Room.class);
        }

        return roomDao;
    }

    public Dao<Schedule, Integer> getSchedulesDao()throws java.sql.SQLException {
        if (schedulesDao== null) {
            schedulesDao= getDao(Schedule.class);
        }

        return schedulesDao;
    }

    public Dao<Subjects, Integer> getSubjectsDao() throws java.sql.SQLException {
        if (subjectsDao == null) {
            subjectsDao = getDao(Subjects.class);
        }

        return subjectsDao;
    }

    public Dao<Teachers, Integer> getTeachersDao() throws java.sql.SQLException {
        if (teachersDao == null) {
            teachersDao= getDao(Teachers.class);
        }

        return teachersDao;
    }

    public Dao<Transacrions, Integer> getTransactionsDao() throws java.sql.SQLException {
        if (transactionsDao == null) {
            transactionsDao = getDao(Transacrions.class);
        }

        return transactionsDao;
    }

    public Dao<QueryMapperPojo, Integer> getQueryMapperPojoIntegerDao() throws java.sql.SQLException{
        if (queryMapperPojos == null) {
            queryMapperPojos= getDao(QueryMapperPojo.class);
        }

        return queryMapperPojos;
    }

}
