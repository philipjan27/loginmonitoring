package alphasoft.com.loginmonitoring.Api;

import java.util.List;

import alphasoft.com.loginmonitoring.Pojo.Departments;
import alphasoft.com.loginmonitoring.Pojo.Employee;
import alphasoft.com.loginmonitoring.Pojo.Room;
import alphasoft.com.loginmonitoring.Pojo.Schedule;
import alphasoft.com.loginmonitoring.Pojo.Subjects;
import alphasoft.com.loginmonitoring.Pojo.Teachers;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by PhilipJan on 7/31/2017.
 */

public interface APIClient {

    @GET("employee")
    Call<List<Employee>> getEmployeeList();

    @GET("room")
    Call<List<Room>> getRoomList();

    @GET("scheduler")
    Call<List<Schedule>> getScheduleList();

    @GET("teacher")
    Call<List<Teachers>> getTeachersList();

    @GET("subject")
    Call<List<Subjects>> getSubjectList();

    @GET("department")
    Call<List<Departments>> getDepartMentList();

}
