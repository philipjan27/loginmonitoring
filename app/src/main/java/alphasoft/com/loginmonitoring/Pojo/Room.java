package alphasoft.com.loginmonitoring.Pojo;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by PhilipJan on 7/30/2017.
 */
@DatabaseTable(tableName = "Room")
public class Room implements Serializable{

    @DatabaseField(generatedId = true)
    @SerializedName("id")
    private int id;

    @DatabaseField(columnName = "roomName")
    @SerializedName("name")
    private String name;

    @DatabaseField(columnName = "roomDescription")
    @SerializedName("description")
    private String descriptiom;

    public Room(int id, String name, String descriptiom) {
        this.id = id;
        this.name = name;
        this.descriptiom = descriptiom;
    }

    public Room() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptiom() {
        return descriptiom;
    }

    public void setDescriptiom(String descriptiom) {
        this.descriptiom = descriptiom;
    }
}
