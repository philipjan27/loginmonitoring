package alphasoft.com.loginmonitoring.Pojo;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by PhilipJan on 7/30/2017.
 */

@DatabaseTable(tableName = "Employee")
public class Employee {

    @DatabaseField(generatedId = true)
    @SerializedName("id")
    private int id;

    @DatabaseField(columnName = "profilepic")
    @SerializedName("profile_picture")
    private String profilPicture;

    @DatabaseField(columnName = "email")
    @SerializedName("email")
    private String email;

    @DatabaseField(columnName = "firstName")
    @SerializedName("first_name")
    private String firstName;

    @DatabaseField(columnName = "lastName")
    @SerializedName("last_name")
    private String lastName;

    @DatabaseField(columnName = "position")
    @SerializedName("position")
    private String position;

    @DatabaseField(columnName = "dateJoined")
    @SerializedName("date_joined")
    private String dateJoined;

    @DatabaseField(columnName = "updatedAt")
    @SerializedName("updated_at")
    private String updatedAt;

    @DatabaseField(columnName = "nickName")
    @SerializedName("nick_name")
    private String nickName;

    @DatabaseField(columnName = "provincialAddress")
    @SerializedName("provincial_address")
    private String provincialAddress;

    @DatabaseField(columnName = "sex")
    @SerializedName("sex")
    private String sex;

    @DatabaseField(columnName = "civilStatus")
    @SerializedName("civil_status")
    private String civilStatus;

    @DatabaseField(columnName = "citizenship")
    @SerializedName("citizenship")
    private String citizenship;

    @DatabaseField(columnName = "religion")
    @SerializedName("religion")
    private String religion;

    @DatabaseField(columnName = "weight", dataType = DataType.DOUBLE)
    @SerializedName("weight")
    private double weight;

    @DatabaseField(columnName = "user")
    @SerializedName("user")
    private int user;

    public Employee(int id, String profilPicture, String email, String firstName, String lastName, String position, String dateJoined, String updatedAt, String nickName, String provincialAddress, String sex, String civilStatus, String citizenship, String religion, double weight, int user) {
        this.id = id;
        this.profilPicture = profilPicture;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.position = position;
        this.dateJoined = dateJoined;
        this.updatedAt = updatedAt;
        this.nickName = nickName;
        this.provincialAddress = provincialAddress;
        this.sex = sex;
        this.civilStatus = civilStatus;
        this.citizenship = citizenship;
        this.religion = religion;
        this.weight = weight;
        this.user = user;
    }

    public Employee() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProfilPicture() {
        return profilPicture;
    }

    public void setProfilPicture(String profilPicture) {
        this.profilPicture = profilPicture;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(String dateJoined) {
        this.dateJoined = dateJoined;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getProvincialAddress() {
        return provincialAddress;
    }

    public void setProvincialAddress(String provincialAddress) {
        this.provincialAddress = provincialAddress;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getCivilStatus() {
        return civilStatus;
    }

    public void setCivilStatus(String civilStatus) {
        this.civilStatus = civilStatus;
    }

    public String getCitizenship() {
        return citizenship;
    }

    public void setCitizenship(String citizenship) {
        this.citizenship = citizenship;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getUser() {
        return user;
    }

    public void setUser(int user) {
        this.user = user;
    }
}
