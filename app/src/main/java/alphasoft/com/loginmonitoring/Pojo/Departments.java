package alphasoft.com.loginmonitoring.Pojo;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.List;

/**
 * Created by PhilipJan on 7/30/2017.
 */

@DatabaseTable(tableName = "Departments")
public class Departments implements Serializable {


    @DatabaseField(generatedId = true)
    @SerializedName("id")
    private int id;

    @DatabaseField(columnName = "departmentName", canBeNull = false)
    @SerializedName("name")
    private String name;

    public Departments(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Departments() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


}
