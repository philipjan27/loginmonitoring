package alphasoft.com.loginmonitoring.Pojo;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by PhilipJan on 7/30/2017.
 */
@DatabaseTable(tableName = "Subjects")
public class Subjects implements Serializable{

    @DatabaseField(generatedId = true)
    @SerializedName("id")
    private int id;

    @DatabaseField(columnName = "offerCode")
    @SerializedName("offer_code")
    private String offer_code;

    @DatabaseField(columnName = "subjectCode")
    @SerializedName("subject_code")
    private String subject_code;

    @DatabaseField(columnName = "subjectDescription")
    @SerializedName("description")
    private String description;

    @DatabaseField(columnName = "departmentId")
    @SerializedName("department")
    private int department;

    @DatabaseField(columnName = "subjectType")
    @SerializedName("subject_type")
    private String subject_type;

    public Subjects(int id, String offer_code, String subject_code, String description, int department, String subject_type) {
        this.id = id;
        this.offer_code = offer_code;
        this.subject_code = subject_code;
        this.description = description;
        this.department = department;
        this.subject_type = subject_type;
    }

    public Subjects() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOffer_code() {
        return offer_code;
    }

    public void setOffer_code(String offer_code) {
        this.offer_code = offer_code;
    }

    public String getSubject_code() {
        return subject_code;
    }

    public void setSubject_code(String subject_code) {
        this.subject_code = subject_code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getDepartment() {
        return department;
    }

    public void setDepartment(int department) {
        this.department = department;
    }

    public String getSubject_type() {
        return subject_type;
    }

    public void setSubject_type(String subject_type) {
        this.subject_type = subject_type;
    }
}
