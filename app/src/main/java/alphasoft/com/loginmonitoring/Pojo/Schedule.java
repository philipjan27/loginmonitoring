package alphasoft.com.loginmonitoring.Pojo;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by PhilipJan on 7/30/2017.
 */
@DatabaseTable(tableName = "Schedule")
public class Schedule{

    @DatabaseField(generatedId = true)
    @SerializedName("id")
    private int id;

    @DatabaseField(columnName = "numberOfStudent")
    @SerializedName("number_of_student")
    private int numberOfStudent;

    @DatabaseField(columnName = "loadType")
    @SerializedName("load_type")
    private String loadType;

    @DatabaseField(columnName = "academicYear")
    @SerializedName("academic_year")
    private String academicYear;

    @DatabaseField(columnName = "semester")
    @SerializedName("semester")
    private String semester;

    @DatabaseField(columnName = "startTime")
    @SerializedName("time_from")
    private String startTime;

    @DatabaseField(columnName = "endTime")
    @SerializedName("time_to")
    private String endTime;

    @DatabaseField(columnName = "day")
    @SerializedName("day")
    private String day;

    @DatabaseField(columnName = "teacherId")
    @SerializedName("teacher")
    private int teacher;

    @DatabaseField(columnName = "subjectId")
    @SerializedName("subject")
    private int subject;

    @DatabaseField(columnName = "roomId")
    @SerializedName("room")
    private int room;

    private Room roomId;

    public Schedule(int id, int numberOfStudent, String loadType, String academicYear, String semester, String startTime, String endTime, String day, int teacher, int subject, int room, Room roomId) {
        this.id = id;
        this.numberOfStudent = numberOfStudent;
        this.loadType = loadType;
        this.academicYear = academicYear;
        this.semester = semester;
        this.startTime = startTime;
        this.endTime = endTime;
        this.day = day;
        this.teacher = teacher;
        this.subject = subject;
        this.room = room;
        this.roomId=roomId;
    }

    public Schedule() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumberOfStudent() {
        return numberOfStudent;
    }

    public void setNumberOfStudent(int numberOfStudent) {
        this.numberOfStudent = numberOfStudent;
    }

    public String getLoadType() {
        return loadType;
    }

    public void setLoadType(String loadType) {
        this.loadType = loadType;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public int getTeacher() {
        return teacher;
    }

    public void setTeacher(int teacher) {
        this.teacher = teacher;
    }

    public int getSubject() {
        return subject;
    }

    public void setSubject(int subject) {
        this.subject = subject;
    }

    public int getRoom() {
        return room;
    }

    public void setRoom(int room) {
        this.room = room;
    }

    public Room getRoomId() {
        roomId.setId(room);
        return roomId;
    }

    public void setRoomId(Room roomId) {
        this.roomId = roomId;
    }
}
