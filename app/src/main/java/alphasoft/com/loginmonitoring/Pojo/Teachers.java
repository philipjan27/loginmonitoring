package alphasoft.com.loginmonitoring.Pojo;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by PhilipJan on 7/30/2017.
 */
@DatabaseTable(tableName = "Teachers")
public class Teachers implements Serializable {

    @DatabaseField(generatedId = true)
    @SerializedName("id")
    private int id;

    @DatabaseField(columnName = "totalHours")
    @SerializedName("total_hours")
    private int totalHours;

    @DatabaseField(columnName = "employeeId")
    @SerializedName("employee")
    private int employeeId;

    public Teachers(int id, int totalHours, int employeeId) {
        this.id = id;
        this.totalHours = totalHours;
        this.employeeId = employeeId;
    }

    public Teachers() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(int totalHours) {
        this.totalHours = totalHours;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }
}
