package alphasoft.com.loginmonitoring.Pojo;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;

/**
 * Created by PhilipJan on 8/9/2017.
 */

@DatabaseTable(tableName = "ScheduleData")
public class QueryMapperPojo implements Serializable {

    @DatabaseField(generatedId = true)
    private int scheduleId;

    @DatabaseField(columnName = "numberOfStudents")
    private int numberOfStudents;

    @DatabaseField(columnName = "loadType")
    private String loadType;

    @DatabaseField(columnName = "academicYear")
    private String academicYear;

    @DatabaseField(columnName = "semester")
    private String semester;

    @DatabaseField(columnName = "startTime")
    private String startTime;

    @DatabaseField(columnName = "endTime")
    private String endTime;

    @DatabaseField(columnName = "day")
    private String day;

    @DatabaseField(columnName = "teacherId")
    private int teacherId;

    @DatabaseField(columnName = "subjectId")
    private int subjectId;

    @DatabaseField(columnName = "roomId")
    private int roomId;

    @DatabaseField(columnName = "roomName")
    private String roomName;

    @DatabaseField(columnName = "subjectCode")
    private String subjectCode;

    @DatabaseField(columnName = "empFirstName")
    private String empFirstName;

    @DatabaseField(columnName = "empLastName")
    private String empLastName;

    public QueryMapperPojo(int scheduleId, int numberOfStudents, String loadType, String academicYear, String semester, String startTime, String endTime, String day, int teacherId, int subjectId, int roomId, String roomName, String subjectCode, String empFirstName, String empLastName) {
        this.scheduleId = scheduleId;
        this.numberOfStudents = numberOfStudents;
        this.loadType = loadType;
        this.academicYear = academicYear;
        this.semester = semester;
        this.startTime = startTime;
        this.endTime = endTime;
        this.day = day;
        this.teacherId = teacherId;
        this.subjectId = subjectId;
        this.roomId = roomId;
        this.roomName = roomName;
        this.subjectCode = subjectCode;
        this.empFirstName = empFirstName;
        this.empLastName = empLastName;
    }

    public QueryMapperPojo() {
    }

    public int getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(int scheduleId) {
        this.scheduleId = scheduleId;
    }

    public int getNumberOfStudents() {
        return numberOfStudents;
    }

    public void setNumberOfStudents(int numberOfStudents) {
        this.numberOfStudents = numberOfStudents;
    }

    public String getLoadType() {
        return loadType;
    }

    public void setLoadType(String loadType) {
        this.loadType = loadType;
    }

    public String getAcademicYear() {
        return academicYear;
    }

    public void setAcademicYear(String academicYear) {
        this.academicYear = academicYear;
    }

    public String getSemester() {
        return semester;
    }

    public void setSemester(String semester) {
        this.semester = semester;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public int getRoomId() {
        return roomId;
    }

    public void setRoomId(int roomId) {
        this.roomId = roomId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getEmpFirstName() {
        return empFirstName;
    }

    public void setEmpFirstName(String empFirstName) {
        this.empFirstName = empFirstName;
    }

    public String getEmpLastName() {
        return empLastName;
    }

    public void setEmpLastName(String empLastName) {
        this.empLastName = empLastName;
    }
}
