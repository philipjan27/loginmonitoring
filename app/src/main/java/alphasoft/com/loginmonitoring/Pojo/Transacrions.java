package alphasoft.com.loginmonitoring.Pojo;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by PhilipJan on 7/31/2017.
 */

@DatabaseTable(tableName = "Transactions")
public class Transacrions implements Serializable {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(columnName = "dateChecked", dataType = DataType.DATE_STRING, format = "yyyy-MM-dd HH:mm:ss")
    private Date checked;

    @DatabaseField(columnName = "transactionTeacherId", foreign = true, foreignAutoRefresh = true)
    private Schedule teacherId;

    @DatabaseField(columnName = "transactionSubjectId", foreign = true, foreignAutoRefresh = true)
    private Schedule subjectId;

    @DatabaseField(columnName = "transactionRoomId", foreign = true, foreignAutoRefresh = true)
    private Schedule roomId;

    public Transacrions(int id, Date checked, Schedule teacherId, Schedule subjectId, Schedule roomId) {
        this.id = id;
        this.checked = checked;
        this.teacherId = teacherId;
        this.subjectId = subjectId;
        this.roomId = roomId;
    }

    public Transacrions() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getChecked() {
        return checked;
    }

    public void setChecked(Date checked) {
        this.checked = checked;
    }

    public Schedule getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Schedule teacherId) {
        this.teacherId = teacherId;
    }

    public Schedule getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(Schedule subjectId) {
        this.subjectId = subjectId;
    }

    public Schedule getRoomId() {
        return roomId;
    }

    public void setRoomId(Schedule roomId) {
        this.roomId = roomId;
    }
}
