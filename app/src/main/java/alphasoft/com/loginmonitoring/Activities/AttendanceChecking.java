package alphasoft.com.loginmonitoring.Activities;

import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import alphasoft.com.loginmonitoring.Fragments.RoomFragment;
import alphasoft.com.loginmonitoring.R;
import alphasoft.com.loginmonitoring.Utilities.Constants;

public class AttendanceChecking extends AppCompatActivity {

    DrawerLayout drawerLayout;
    NavigationView nv;
    ActionBarDrawerToggle toggle;
    View headerLayout;
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendance_checking);

        nv = (NavigationView) findViewById(R.id.navigationView);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        setupDrawer();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(null);
        toolbar.setContentInsetsAbsolute(0, 0);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        toggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_opened, R.string.drawer_closed);
        drawerLayout.addDrawerListener(toggle);

        toggle.setDrawerIndicatorEnabled(true);
        drawerLayout.openDrawer(Gravity.LEFT);
    }

    private void setupDrawer() {
        nv.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                selectItem(item);
                return false;
            }
        });
    }

    private void selectItem(MenuItem item) {
        String building = "";
        RoomFragment fragment=null;
        Class fragmentClass=null;

        switch (item.getItemId()) {

            case R.id.buildinga_secondfloor:
                building = Constants.BUILDINGA_2NDFLR;
                break;
            case R.id.buildinga_thirdfloor:
                building = Constants.BUILDINGA_3RDFLR;
                break;
            case R.id.buildinga_fourthfloor:
                building = Constants.BUILDINGA_4THFLR;
                break;
            case R.id.buildinga_fifthfloor:
                building = Constants.BUILDINGA_5THFLR;
                break;
            case R.id.buildingb_secondfloor:
                building = Constants.BUILDINGB_2NDFLR;
                break;
            case R.id.buildingb_thirdfloor:
                building = Constants.BUILDINGB_3RDFLR;
                break;
            case R.id.buildingb_fourthfloor:
                building = Constants.BUILDINGB_4THFLR;
                break;
            case R.id.buildingb_fifthfloor:
                building = Constants.BUILDINGB_5THFLR;
                break;
            case R.id.buildingb_sixthfloor:
                building = Constants.BUILDINGB_6THFLR;
                break;
            case R.id.buildingb_seventfloor:
                building = Constants.BUILDINGB_7THFLR;
                break;
            case R.id.buildingc_secondfloor:
                building = Constants.BUILDINGC_2NDFLR;
                break;
            case R.id.buildingc_thirdfloor:
                building = Constants.BUILDINGC_3RDFLR;
                break;
            case R.id.buildingc_fourthfloor:
                building = Constants.BUILDINGC_4THFLR;
                break;
            case R.id.buildingc_fifthfloor:
                building = Constants.BUILDINGC_5THFLR;
                break;
            case R.id.buildingc_sixthfloor:
                building = Constants.BUILDINGC_6THFLR;
                break;
            case R.id.buildingd_secondfloor:
                building = Constants.BUILDINGD_2NDFLR;
                break;
            case R.id.buildingd_thirdfloor:
                building = Constants.BUILDINGD_3RDFLR;
                break;
            case R.id.buildingd_fourthfloor:
                building = Constants.BUILDINGD_4THFLR;
                break;
            case R.id.buildingd_fifthfloor:
                building = Constants.BUILDINGD_5THFLR;
                break;
            case R.id.buildingd_sixthfloor:
                building = Constants.BUILDINGD_6THFLR;
                break;
            case R.id.otherbuildings_firstfloor:
                building = Constants.BUILDINGE_OTHERFLOOR;
                break;

            default:
                item.getItemId();
                break;
        }

        try {
            Bundle bundle = new Bundle();
            bundle.putString("room", building);
            
            fragment =new RoomFragment();

            fragment.setArguments(bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }


        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.framelayout_searchClasses, fragment).commit();


        item.setChecked(true);
        setTitle(item.getTitle());
        drawerLayout.closeDrawers();
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onPostCreate(savedInstanceState, persistentState);
        toggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
