package alphasoft.com.loginmonitoring.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;

import java.util.ArrayList;
import java.util.List;

import alphasoft.com.loginmonitoring.DatabaseUtils.DBUtils;
import alphasoft.com.loginmonitoring.Pojo.Employee;
import alphasoft.com.loginmonitoring.Pojo.Schedule;
import alphasoft.com.loginmonitoring.R;
import alphasoft.com.loginmonitoring.Utilities.Utils;

public class MainActivity extends AppCompatActivity {

    Button btnLogin;
    EditText username,password;
    PopupWindow window;
    SharedPreferences prefs;
    DBUtils dbUtils;
    Utils utils;

    List<Employee> employeeList;
    List<Schedule> scheduleList;

    private final static String TAG= MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbUtils= new DBUtils(getApplicationContext());

        employeeList= new ArrayList<>();
        employeeList= Utils.getEmployees(getApplicationContext());
        Log.d(TAG, "Employees size from DB "+employeeList.size());
        Log.d(TAG, "Employees: "+employeeList.get(0).getFirstName()+" "+employeeList.get(0).getLastName());
      //  Log.d(TAG, "Employees: "+employeeList.get(1).getDateJoined()+" "+employeeList.get(1).getUpdatedAt());

        scheduleList= new ArrayList<>();
        scheduleList= Utils.getSchedulesPerDay(getApplicationContext());
        Log.d(TAG,"Schedules per day from DB "+ scheduleList.size());

        prefs= Utils.initSharedPrefs(this);
        username= (EditText)findViewById(R.id.login_usernameEditext);
        password= (EditText)findViewById(R.id.login_passwordEditext);

        btnLogin=(Button) findViewById(R.id.login_buttonSbmit);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              /**  LayoutInflater inflater= (LayoutInflater)getApplicationContext().getSystemService(LAYOUT_INFLATER_SERVICE);
                View v= inflater.inflate(R.layout.checkstatus_popupdialog,null);

                window= new PopupWindow(v, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                window.showAsDropDown(btnLogin,50, -30); */
                String uName= username.getText().toString();
                String pWord= password.getText().toString();

                Utils.saveCredToSharedPrefs(prefs,uName,pWord);
                boolean isSavedOrIsEmpty= Utils.checkCredIfAvailable(prefs,getApplicationContext());

                Log.e(MainActivity.class.getSimpleName(), "SharedPrefs empty ? : "+isSavedOrIsEmpty);

                startActivity(new Intent(MainActivity.this, AttendanceChecking.class));
            }
        });

    }


}
