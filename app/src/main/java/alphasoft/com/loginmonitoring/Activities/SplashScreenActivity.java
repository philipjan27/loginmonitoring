package alphasoft.com.loginmonitoring.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.SQLException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

import alphasoft.com.loginmonitoring.DatabaseUtils.DBHelper;
import alphasoft.com.loginmonitoring.DatabaseUtils.DBUtils;
import alphasoft.com.loginmonitoring.R;

public class SplashScreenActivity extends AppCompatActivity {

    DBUtils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        final ProgressDialog dialog= new ProgressDialog(this);

        utils= new DBUtils(getApplicationContext());


        Thread t=new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.d(SplashScreenActivity.class.getSimpleName(),"Sleeping for 5 Secs");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.setMessage("Checking local databases..");
                            dialog.show();
                        }
                    });

                    Thread.sleep(3000);
                    Intent i= new Intent(SplashScreenActivity.this, MainActivity.class);
                    startActivity(i);
                }catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        });

        t.start();

       runOnUiThread(new Runnable() {
           @Override
           public void run() {
               utils.initializeAllRequest();
           }
       });


        if (dialog.isShowing()) {
            dialog.dismiss();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
