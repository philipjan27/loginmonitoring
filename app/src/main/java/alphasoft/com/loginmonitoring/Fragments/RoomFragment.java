package alphasoft.com.loginmonitoring.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import alphasoft.com.loginmonitoring.Pojo.QueryMapperPojo;
import alphasoft.com.loginmonitoring.Pojo.Schedule;
import alphasoft.com.loginmonitoring.R;
import alphasoft.com.loginmonitoring.Utilities.Utils;

/**
 * A simple {@link Fragment} subclass.
 */
public class RoomFragment extends Fragment {


    private final static String TAG= RoomFragment.class.getSimpleName();
    List<QueryMapperPojo> filteredSchedulesBasedOnFloors;
    String room;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate()");

        room= getArguments().getString("room");
        Log.d(getClass().getSimpleName(), "Building: "+room);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_room, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG,"onResume()");

        filteredSchedulesBasedOnFloors= Utils.getSchedulesBasedOnFloors(getActivity(),room);
        Log.d(TAG,"Results size: "+filteredSchedulesBasedOnFloors.size());
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy()");
    }
}
