package alphasoft.com.loginmonitoring.Utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.dao.RawRowMapper;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;


import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import alphasoft.com.loginmonitoring.Pojo.Departments;
import alphasoft.com.loginmonitoring.Pojo.Employee;
import alphasoft.com.loginmonitoring.Pojo.QueryMapperPojo;
import alphasoft.com.loginmonitoring.Pojo.Room;
import alphasoft.com.loginmonitoring.Pojo.Schedule;
import alphasoft.com.loginmonitoring.Pojo.Subjects;
import alphasoft.com.loginmonitoring.Pojo.Teachers;
import alphasoft.com.loginmonitoring.Pojo.Transacrions;
import alphasoft.com.loginmonitoring.DatabaseUtils.DBHelper;


/**
 * Created by PhilipJan on 7/25/2017.
 */

public class Utils {
    Context ctx;

    public Utils(Context context) {
        this.ctx=context;
    }

    public static SharedPreferences initSharedPrefs(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences(Prefs.PREF_FILENAME, Context.MODE_PRIVATE);
        return prefs;
    }

    public static void saveCredToSharedPrefs(SharedPreferences prefs, String username, String password) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(Prefs.PREF_USERNAME, username);
        editor.putString(Prefs.PREF_PASSWORD, password);
        editor.commit();
    }

    public static boolean checkCredIfAvailable(SharedPreferences prefs, Context ctx) {
        prefs = initSharedPrefs(ctx);
        if (prefs != null) {
            String usrname = prefs.getString(Prefs.PREF_USERNAME, "");
            String passwrd = prefs.getString(Prefs.PREF_PASSWORD, "");

            if (usrname.isEmpty() && passwrd.isEmpty()) {
                return false;
            } else {

                Log.d(Utils.class.getSimpleName(), "Username: " + usrname + " Password: " + passwrd);
                return true;
            }
        }

        return false;
    }

    public static String getCurrentDay() {
        Calendar calendar= Calendar.getInstance();
        Date date= calendar.getTime();

        return new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime());
    }

    public static Date getCurrentTimeStamp() {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date dateProductCreated = new Date();
        sdf.format(dateProductCreated);
        return dateProductCreated;
    }

    public static DBHelper getDBHelper(Context ctx) {
        DBHelper helper = null;

        if (helper == null) {
            helper = OpenHelperManager.getHelper(ctx, DBHelper.class);
        }

        return helper;
    }

    // Departments Dao
    public static void saveDepartmentsToDb(List<Departments> departmentses, Context ctx) {

        Dao<Departments, Integer> departmentsIntegerDao;

        for (int i = 0; i < departmentses.size(); i++) {
            Departments departments = new Departments();
            departments.setId(departmentses.get(i).getId());
            departments.setName(departmentses.get(i).getName());

            try {
                departmentsIntegerDao = getDBHelper(ctx).getDepartmentsDao();
                departmentsIntegerDao.createIfNotExists(departments);
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    // Employees Dao
    public static void saveEmployeeToDb(List<Employee> employeesList, Context ctx) {

        Dao<Employee, Integer> employeeIntegerDao;

        for (int i = 0; i < employeesList.size(); i++) {
            Employee employee = new Employee();
            employee.setId(employeesList.get(i).getId());
            employee.setCitizenship(employeesList.get(i).getCitizenship());
            employee.setCivilStatus(employeesList.get(i).getCivilStatus());
            employee.setDateJoined(employeesList.get(i).getDateJoined());
            employee.setEmail(employeesList.get(i).getEmail());
            employee.setFirstName(employeesList.get(i).getFirstName());
            employee.setLastName(employeesList.get(i).getLastName());
            employee.setNickName(employeesList.get(i).getNickName());
            employee.setPosition(employeesList.get(i).getPosition());
            employee.setProfilPicture(employeesList.get(i).getProfilPicture());
            employee.setProvincialAddress(employeesList.get(i).getProvincialAddress());
            employee.setReligion(employeesList.get(i).getReligion());
            employee.setSex(employeesList.get(i).getSex());
            employee.setUpdatedAt(employeesList.get(i).getUpdatedAt());
            employee.setUser(employeesList.get(i).getUser());
            employee.setWeight(employeesList.get(i).getWeight());

            try {
                employeeIntegerDao = getDBHelper(ctx).getEmployeeDao();
                employeeIntegerDao.createIfNotExists(employee);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    // Rooms Dao
    public static void saveRoomToDb(List<Room> roomList, Context ctx) {
        Dao<Room, Integer> roomIntegerDao;

        for (int i=0; i<roomList.size(); i++) {
            Room room= new Room();
            room.setId(roomList.get(i).getId());
            room.setName(roomList.get(i).getName());
            room.setDescriptiom(roomList.get(i).getDescriptiom());

            try {
                roomIntegerDao= getDBHelper(ctx).getRoomDao();
                roomIntegerDao.createIfNotExists(room);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    // Schedule Dao
    public static void saveScheduleToDb(List<Schedule> schedulesList, Context ctx) {

        Dao<Schedule, Integer> scheduleIntegerDao;

        for (int i=0; i< schedulesList.size(); i++) {
            Schedule schedule= new Schedule();
            schedule.setId(schedulesList.get(i).getId());
            schedule.setAcademicYear(schedulesList.get(i).getAcademicYear());
            schedule.setDay(schedulesList.get(i).getDay());
            schedule.setEndTime(schedulesList.get(i).getEndTime());
            schedule.setLoadType(schedulesList.get(i).getLoadType());
            schedule.setNumberOfStudent(schedulesList.get(i).getNumberOfStudent());
            schedule.setRoom(schedulesList.get(i).getRoom());
            schedule.setSemester(schedulesList.get(i).getSemester());
            schedule.setStartTime(schedulesList.get(i).getStartTime());
            schedule.setSubject(schedulesList.get(i).getSubject());
            schedule.setTeacher(schedulesList.get(i).getTeacher());

            try {
                scheduleIntegerDao= getDBHelper(ctx).getSchedulesDao();
                scheduleIntegerDao.createIfNotExists(schedule);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

    // Subjects
    public static void saveSubjectsToDb(List<Subjects>subjectsList, Context ctx) {

        Dao<Subjects, Integer> subjectsIntegerDao;

        for (int i=0; i<subjectsList.size(); i++) {
            Subjects subjects= new Subjects();
            subjects.setId(subjectsList.get(i).getId());
            subjects.setDepartment(subjectsList.get(i).getDepartment());
            subjects.setDescription(subjectsList.get(i).getDescription());
            subjects.setOffer_code(subjectsList.get(i).getOffer_code());
            subjects.setSubject_code(subjectsList.get(i).getSubject_code());
            subjects.setSubject_type(subjectsList.get(i).getSubject_type());

            try {
                subjectsIntegerDao= getDBHelper(ctx).getSubjectsDao();
                subjectsIntegerDao.createIfNotExists(subjects);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    // Teacher
    public static void saveTeacherToDb(List<Teachers> teachersList, Context ctx) {
        Dao<Teachers, Integer> teachersIntegerDao;

        for (int i=0; i<teachersList.size(); i++) {
            Teachers teachers= new Teachers();
            teachers.setId(teachersList.get(i).getId());
            teachers.setEmployeeId(teachersList.get(i).getEmployeeId());
            teachers.setTotalHours(teachersList.get(i).getTotalHours());

            try {
                teachersIntegerDao= getDBHelper(ctx).getTeachersDao();
                teachersIntegerDao.createIfNotExists(teachers);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    // Transactions
    public static void saveTransaction(Context ctx, Schedule schedule) {
        Dao<Transacrions, Integer> transacrionsIntegerDao;

       schedule= new Schedule();
        Transacrions transactions= new Transacrions();
        transactions.setChecked(getCurrentTimeStamp());
        transactions.setRoomId(schedule);
        transactions.setSubjectId(schedule);
        transactions.setTeacherId(schedule);

        try {
            transacrionsIntegerDao= getDBHelper(ctx).getTransactionsDao();
            transacrionsIntegerDao.createIfNotExists(transactions);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    // Getting Every day schedule
    public static List<Schedule> getSchedulesPerDay(Context ctx) {

        String currentDay=getCurrentDay();
        Dao<Schedule, Integer> scheduleIntegerDao;
        List<Schedule> weekSchedulesList= new ArrayList<>();
        List<Schedule> schedulesPerday= new ArrayList<>();

       try {
           scheduleIntegerDao= getDBHelper(ctx).getSchedulesDao();
           weekSchedulesList= scheduleIntegerDao.queryForAll();
       } catch (SQLException e) {
           e.printStackTrace();
       }

        for (int i=0; i< weekSchedulesList.size(); i++) {
            if (weekSchedulesList.get(i).getDay().equalsIgnoreCase(currentDay)) {
                schedulesPerday.add(weekSchedulesList.get(i));
            }
        }

        return schedulesPerday;

    }

    // get list of schedules based on floors clicked in navigation view
    public static List<QueryMapperPojo> getSchedulesBasedOnFloors(Context ctx, String likeIndex) {

        List<QueryMapperPojo>queryMapperPojosList= new ArrayList<>();

        GenericRawResults<QueryMapperPojo> queryMapperPojos;
        String q= "SELECT Schedule.id, Schedule.numberOfStudent, Schedule.loadType, Schedule.academicYear, Schedule.semester, Schedule.startTime, Schedule.endTime, Schedule.day, Schedule.teacherId, Schedule.subjectId, Schedule.roomId, Room.roomName, Subjects.subjectCode, Employee.firstName, Employee.lastName " +
                "FROM Schedule LEFT OUTER JOIN Employee ON Schedule.teacherId = Employee.id" +
                " LEFT OUTER JOIN Subjects ON Schedule.subjectId = Subjects.id" +
                " LEFT OUTER JOIN Room ON Schedule.roomId = Room.id";// +
              //  " WHERE Room.roomName LIKE '"+likeIndex+"%'";

        RawRowMapper<QueryMapperPojo> queryMapperPojoRawRowMapper= new RawRowMapper<QueryMapperPojo>() {
            @Override
            public QueryMapperPojo mapRow(String[] columnNames, String[] resultColumns) throws SQLException {

                Log.d(getClass().getSimpleName(), "resultColumns size: "+resultColumns.length);
                Log.d(getClass().getSimpleName(), "columnNames size: "+columnNames.length);
                return new QueryMapperPojo();
            }
        };

        try {
            queryMapperPojos= getDBHelper(ctx).getQueryMapperPojoIntegerDao().queryRaw(q,queryMapperPojoRawRowMapper);
            queryMapperPojosList= queryMapperPojos.getResults();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return queryMapperPojosList;
    }

    public static List<Employee> getEmployees(Context ctx) {
        List<Employee> employeeList= new ArrayList<>();
        Dao<Employee, Integer> employeeIntegerDao;

        try {
            employeeIntegerDao= getDBHelper(ctx).getEmployeeDao();
            employeeList= employeeIntegerDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return employeeList;
    }

}
