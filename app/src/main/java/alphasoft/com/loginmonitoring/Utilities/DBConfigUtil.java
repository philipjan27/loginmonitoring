package alphasoft.com.loginmonitoring.Utilities;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import alphasoft.com.loginmonitoring.Pojo.Departments;
import alphasoft.com.loginmonitoring.Pojo.Employee;
import alphasoft.com.loginmonitoring.Pojo.QueryMapperPojo;
import alphasoft.com.loginmonitoring.Pojo.Room;
import alphasoft.com.loginmonitoring.Pojo.Schedule;
import alphasoft.com.loginmonitoring.Pojo.Subjects;
import alphasoft.com.loginmonitoring.Pojo.Teachers;
import alphasoft.com.loginmonitoring.Pojo.Transacrions;


/**
 * Created by PhilipJan on 7/31/2017.
 */

public class DBConfigUtil extends OrmLiteConfigUtil {

    private static final Class<?>[] classes= new Class[] {
            Departments.class,Employee.class, Room.class, Schedule.class, Subjects.class, Teachers.class, Transacrions.class, QueryMapperPojo.class
    };

    public static void main(String[] args) throws SQLException, IOException {
        writeConfigFile(new File("C:/Users/PhilipJan/Documents/Git Extensions/LoginMonitoring/app/src/main/res/raw/ormlite_config.txt"),classes);
    }

}
