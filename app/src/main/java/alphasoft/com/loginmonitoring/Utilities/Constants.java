package alphasoft.com.loginmonitoring.Utilities;

import android.database.sqlite.SQLiteDatabase;

import java.sql.Statement;

/**
 * Created by PhilipJan on 7/25/2017.
 */

public class Constants {

    public final static String BASE_URL="http://54.179.154.66/api/";

    public final static String DBNAME="jpc.db";
    public final static int DBVERSION=1;

    public final static int PRESENT= 1;
    public final static int ABSENT= 0;

    public final static String BUILDINGA_2NDFLR= "A2";
    public final static String BUILDINGA_3RDFLR= "A3";
    public final static String BUILDINGA_4THFLR= "A4";
    public final static String BUILDINGA_5THFLR= "A5";

    public final static String BUILDINGB_2NDFLR= "B2";
    public final static String BUILDINGB_3RDFLR= "B3";
    public final static String BUILDINGB_4THFLR= "B4";
    public final static String BUILDINGB_5THFLR= "B5";
    public final static String BUILDINGB_6THFLR= "B6";
    public final static String BUILDINGB_7THFLR= "B7";


    public final static String BUILDINGC_2NDFLR= "C2";
    public final static String BUILDINGC_3RDFLR= "C3";
    public final static String BUILDINGC_4THFLR= "C4";
    public final static String BUILDINGC_5THFLR= "C5";
    public final static String BUILDINGC_6THFLR= "C6";


    public final static String BUILDINGD_2NDFLR= "D2";
    public final static String BUILDINGD_3RDFLR= "D3";
    public final static String BUILDINGD_4THFLR= "D4";
    public final static String BUILDINGD_5THFLR= "D5";
    public final static String BUILDINGD_6THFLR= "D6";

    public final static String BUILDINGE_OTHERFLOOR= "";








}
